<?php
function recursiveCopy($src, $dest, $chmod=0755) {
	echo "\n".$src.'->'.$dest."\n";
    if (is_dir($src)){
        $dir = opendir($src);
    }
    while ($file = readdir($dir)) {
        if ($file != '.' && $file != '..') {
            if (!is_dir($src . '/' . $file)){
                copy($src . '/' . $file, $dest . '/' . $file);
            }else {
                @mkdir($dest . '/' . $file, $chmod);
                recursiveCopy($src . '/' . $file, $dest . '/' . $file);
            } //else
        } //if
    } //while
    closedir($dir);
}
$base_path = realpath(dirname(__FILE__));
$pasta_loja = 'loja';
$chmod = 0755;
$pastas_recursivas = array('app','skin');
foreach($pastas_recursivas as $pasta){
    recursiveCopy($base_path.'/'.$pasta,$base_path.'/../'.$pasta_loja.'/'.$pasta);
}