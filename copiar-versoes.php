<?php
function recursiveCopy($src, $dest, $chmod=0755) {
    if (is_dir($src)){
        $dir = @opendir($src);
    }
    while ($file = @readdir($dir)) {
        if ($file != '.' && $file != '..') {
            if (!is_dir($src . '/' . $file)){
                @copy($src . '/' . $file, $dest . '/' . $file);
            }else {
                @mkdir($dest . '/' . $file, $chmod);
                recursiveCopy($src . '/' . $file, $dest . '/' . $file);
            } //else
        } //if
    } //while
    if($dir){
        @closedir($dir);
    }
}
$base_path = realpath(dirname(__FILE__));
$pasta_loja = 'loja';
$pasta_versoes = 'versoes';
$chmod = 0755;
$pastas_recursivas = array('app/design/frontend/personalizado','skin/frontend/personalizado');
foreach($pastas_recursivas as $pasta){
    //echo "cascata para $pasta<br/>";
    @mkdir($base_path.'/'.$pasta_versoes.'/'.$pasta,$chmod,true);//diretorios recursivos igual ao alvo
    recursiveCopy($base_path.'/'.$pasta_loja.'/'.$pasta,$base_path.'/'.$pasta_versoes.'/'.$pasta);//a partir do diretorio cascateado, leva os arquivos
} 